import React, { useState, useEffect } from 'react';
import ReactFlagsSelect from 'react-flags-select';
import { useSelector, useDispatch } from 'react-redux';
import { RiCalendar2Line } from 'react-icons/ri';
import PropTypes from 'prop-types';
import { Countries } from '../redux/actions/covidData';
import AllDetails from './allDetails';

const Header = () => {
  const [selectedCountry, setSelectedCountry] = useState(
    []
  );
  const [selected, setSelected] = useState([
    { country: 'Rwanda' },
  ]);
  const { countries } = useSelector((state) => {
    return state.countries;
  });
  console.log('countries', countries);
  const dispatch = useDispatch();
  console.log(selected);
  useEffect(() => {
    dispatch(Countries());
  }, [dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setSelectedCountry(
      countries.filter(
        (country) => country.country === selected
      )
    );
  };
  return (
    <div className="bg-main">
      <h1 className="h1 white">UPDATES</h1>
      <p className="small">search a country</p>
      <form
        className="search section-one--form"
        onSubmit={handleSubmit}
      >
        <div classname="countries">
          <select
            className="select-country"
            onChange={(e) => setSelected(e.target.value)}
          >
            <option value={''}>Select country</option>
            {countries.map((country) => (
              <option
                value={country.country}
                key={country.country}
                required
              >
                {country.country}
              </option>
            ))}
          </select>
        </div>
        <input
          type="date"
          style={{ cursor: 'pointer' }}
          required
        />
        <RiCalendar2Line
          style={{
            fontSize: '64px',
            opacity: '0.7',
            cursor: 'pointer',
            marginRight: '4px',
          }}
        />
        <button type="submit" className="button submitBtn">
          SUBMIT
        </button>
      </form>
      {selectedCountry.map((total) => {
        return (
          <div className="total">
            <p className="larger">{total.cases}</p>
            <p className="h1 white">Cumulatively</p>
          </div>
        );
      })}
      {selectedCountry.map((country) => {
        return <AllDetails country={country} />;
      })}
    </div>
  );
};
Header.propTypes = {
  allCases: PropTypes.number.isRequired,
};
Header.defaultProps = {
  allCases: 0,
};
export default Header;
