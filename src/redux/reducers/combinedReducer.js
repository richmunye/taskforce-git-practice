import { combineReducers } from 'redux';
import { allCasesReducer, countries } from './covidReducer';

const rootReducer = combineReducers({
  allCasesReducer,
  countries,
});
export default rootReducer;
